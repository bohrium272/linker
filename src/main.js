import patternToHandlers from './handlers.js';

browser.contextMenus.removeAll();

browser.contextMenus.create({
    "title": "Open directly with Linker",
    "type": "normal",
    "id": "linker-context-menu-1",
    "contexts": ["all"]
});

browser.contextMenus.onClicked.addListener(function (info, tab) {
    for (const [regex, handler] of patternToHandlers.entries()) {
        if (regex.test(info.linkUrl)) {
            var code = handler(info.linkUrl);
            var resultPromise = browser.tabs.executeScript(tab.Id, {code: code});
            resultPromise.then(
                function(result) {
                    openURL(result[0]);
                }
            )
            break;
        }
    }
});

function openURL(url) {
    if (!url.startsWith('http://') && !url.startsWith('https://')) { 
        url = 'http://' + url; 
    };
    browser.tabs.create({active: false, url: url});
}