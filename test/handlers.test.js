import { twitter, youtube, instagram, defaultHandler } from '../src/handlers.js';
import assert from 'assert';

describe('Handlers should work', function() {
    describe('#twitter()', function() {
        it('Should return the correct code', function() {
            var expectedCode = `
                var elements = document.querySelectorAll("a[href='https://t.com/zxcvq1233']");
                var url = '';
                for (i = 0; i < elements.length; i++) {
                    url = elements[i].title == "" ? elements[i].text : elements[i].title;
                    if (url.startsWith("http://") || url.startsWith("https://")) {
                        break;
                    }
                }
                url;
            `
            var actualCode = twitter('https://t.com/zxcvq1233')
            
            assert.equal(cleanup(actualCode), cleanup(expectedCode));
        });
    });

    describe('#youtube()', function() {
        it('Should return the correct code', function() {
            var expectedCode = `
                var elements = document.querySelectorAll("a[href='/redirect?event=video_description&q=abcd&redir_token=efg&v=xyz']");
                var url = elements[0].text;url;
            `
            
            var actualCode = youtube('https://www.youtube.com/redirect?event=video_description&q=abcd&redir_token=efg&v=xyz')
            
            assert.equal(cleanup(actualCode), cleanup(expectedCode));
        });
    });

    describe('#instagram()', function() {
        it('Should return the correct code', function() {
            var expectedCode = `
                var elements = document.querySelectorAll("a[href='https://l.instagram.com/abcd']");
                var url = elements[0].text;url;
            `
            
            var actualCode = instagram('https://l.instagram.com/abcd')
            
            assert.equal(cleanup(actualCode), cleanup(expectedCode));
        });
    });

    describe('#defaultHandler()', function() {
        it('Should return the correct code', function() {
            var expectedCode = `
                var url = 'http://abcd.com'; url;
            `
            
            var actualCode = defaultHandler('http://abcd.com')
            
            assert.equal(cleanup(actualCode), cleanup(expectedCode));
        });
    });
});

function cleanup(code) {
    return code.replace(/\s/g, '');
}
