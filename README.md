![](assets/unlink_material.png)

# Linker

A simple browser extension that adds a context menu option to open links directly on certain websites, circumventing redirect links.

Examples:
* Tweets containing links that reference to `https://t.co`
* Links on Instagram (bio/posts) that reference to `https://l.instagram.com`
* Links on YouTube that reference to `https://www.youtube.com/redirect`


![](assets/demo.gif)


# Installation

Building this extension requires `npm` which should install as part of [Node.js](https://nodejs.org/en/download/)

Then from the project's root directory: 
1. Run `npm install` and then `npm run build`
2. (Optional) Run tests `npm run test`

Add as an add-on/extension in your browser:

##### Chrome
1. Open `chrome://extensions`
2. Enable debugging mode
3. Use "Load unpacked extension" to select the `dist/` folder.

##### Firefox
1. Open `about:debugging`
2. Choose "This Firefox"
3. Use "Load temporary add-on" to select the `manifest.json` file.

## Create .zip

Use `./package.sh` or `sh package.sh` to create a `.zip` in the `extension/` directory.

---
_Logo from [Material Icons](https://material.io/resources/icons/?search=link&icon=link_off&style=outline)_
