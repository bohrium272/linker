export function defaultHandler(linkURL) {
    return `
        var url = '${linkURL}'; url;
    `
}

export function twitter(linkURL) {
    var selector = `a[href='${linkURL}']`;
    var code = `
        var elements = document.querySelectorAll("${selector}");
        var url = '';
        for (i = 0; i < elements.length; i++) {
            url = elements[i].title == "" ? elements[i].text : elements[i].title;
            if (url.startsWith("http://") || url.startsWith("https://")) {
                break;
            }
        }
        url;
    `
    return code;
}

export function youtube(linkURL) {
    var searchStr = 'https://www.youtube.com'
    var queryUrl = linkURL.substr(linkURL.search(searchStr) + searchStr.length);
    var selector = `a[href='${queryUrl}']`;
    var code = `
        var elements = document.querySelectorAll("${selector}");
        var url = elements[0].text;url;
    `
    return code;
}

export function instagram(linkURL) {
    var selector = `a[href='${linkURL}']`;
    var code = `
        var elements = document.querySelectorAll("${selector}");
        var url = elements[0].text;url;
    `
    return code;
}

let patternHandlers = [
    [RegExp('^(http:\/\/t.co)'), twitter],
    [RegExp('^(https:\/\/t.co)'), twitter],
    [RegExp('^(https:\/\/www.youtube.com\/redirect)'), youtube],
    [RegExp('^(https:\/\/l.instagram.com\/)'), instagram],
    [RegExp('.'), defaultHandler]
]

let patternToHandlers = new Map(patternHandlers);
export default patternToHandlers;
