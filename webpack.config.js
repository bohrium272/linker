const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './src/main.js',
    output: {
        filename: 'background.js',
        path: path.resolve(__dirname, 'dist')
    },
    optimization: {
        minimize: true
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: './node_modules/webextension-polyfill/dist/browser-polyfill.js'},
            {from: 'assets/unlink_material.png', to: 'logo.png'}
        ])
    ],
    mode: 'none'
}
